import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import PizzaForm from './pages/PizzaForm';
import Component from './components/Component';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList(data),
	aboutPage = new Component('section', null, 'Ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data; // appel du setter
Router.navigate('/'); // affiche la liste des pizzas

const logoHTML = document.querySelector('.logo');
logoHTML.innerHTML += "<small>les pizzas c'est la vie</small>";

const laCarte = document.querySelector('.pizzaListLink');
laCarte.setAttribute('class', 'pizzaListLink active');

const visibility = document.querySelector('.newsContainer');
// visibility.setAttribute('display', '');

// visibility.removeAttribute('style');

function changeVisibility(elem) {
	if (elem.style.display != '') {
		elem.style.display = '';
	} else {
		elem.style.display = 'none';
	}
}

changeVisibility(visibility);

const closeButton = document.querySelector('.newsContainer');
closeButton.addEventListener('click', event => {
	event.preventDefault();
	changeVisibility(closeButton);
});
